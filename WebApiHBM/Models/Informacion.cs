﻿namespace WebApiHBM.Models
{
    public class Informacion
    {
        public int Estado { get; set; }
        public string Nombre { get; set; }
        public string Division { get; set; }
        public string CodUsuario { get; set; }
        public string Regional { get; set; }
        public string Cargo { get; set; }
        public string DivisionID { get; set; }
        public string Idamercado { get; set; }
        public string Amercado { get; set; }
        public string Organizacion { get; set; }
        public string RolID { get; set; }
        public string CodigoSap { get; set; }
    }
}