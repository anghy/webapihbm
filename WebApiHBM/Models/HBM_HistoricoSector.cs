﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Historial.Models
{
    public class HBM_HistoricoSector
    {
        public string HistoricoID { get; set; }
        public int Estado { get; set; }
        public string Fecha { get; set; }
        public int Documento { get; set; }
        public string CodUsuario { get; set; }
        public string Obs { get; set; }
        public string EstadoID { get; set; }
        public string NombreEstado { get; set; }
        public string PedidoID { get; set; }
        public string RazonSocial { get; set; }
    
    }

    public class HBM_HistoricoPedido
    {
        public string HistoricoID { get; set; }
        public string Estado { get; set; }
        public string Fecha { get; set; }
        public string Documento { get; set; }
        public string NroSAP { get; set; }
        public string CodUsuario { get; set; }
        public string CodUsuarioSAP { get; set; }
        public string Obs { get; set; }
        public string PedidoID { get; set; }
        public string NroPedido { get; set; }
        public string Cliente { get; set; }
        public string Caso { get; set; }

    }
}